require 'uri'
module Chekr
  module Validator
    extend self

    def valid_url?(url)
      !url.match(/https?:\/\/[\S]+/).nil?
    end

    def valida_period?(period)
       period > 0
    end
  end
end
