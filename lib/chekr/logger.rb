module Chekr
  module Logger
    extend self

    ERROR = 31
    SUCCESS = 32
    NORMAL = 0

    def present(data)

      puts <<-REPORT_TIME
      Report:
      Request time:
      Average:#{data[:average_time]}s\tMax:#{data[:max_time]}s\tMin:#{data[:min_time]}s

      HTTP Status Codes:
      #{show_status_report(data[:statuses])}
      REPORT_TIME
    end

    def info_url(url)
      puts "Checking status of #{color_string(SUCCESS,url)}"
    end

    def error_url(url)
      puts "#{color_string(ERROR, url)} is not a valid URL"
    end

    def error_on_process
      puts color_string(ERROR, "An error has occurred, please check all parameters are correct")
    end

    def info_request(hsh)
      message = "Response time #{hsh[:time]}s with status"
      status = case hsh[:status].to_i
      when 200..299
        color_string(SUCCESS,hsh[:status])
      when 400..599
        color_string(ERROR,hsh[:status])
      else
        color_string(NORMAL,hsh[:status])
      end
      puts "#{message} #{status}"
    end

    def color_string(color_code, value)
      "\e[#{color_code}m#{value}\e[0m"
    end

    def show_status_report(status_data)
      status_data.map{ |k,v| "#{k}: #{v} Times"}.join("\n")
    end

  end
end
