require "chekr/version"
require 'chekr/logger'
require 'chekr/requester'
require 'chekr/validator'

require 'thor'

module Chekr
  class CLI < Thor
    desc 'check URL', 'check a website status for a period of time'
    option :interval, type: :numeric, default: 1, desc: 'Total request interval time (in minutes)'
    option :period, type: :numeric, default: 10,  desc: 'Request wait time (in secs)'
    option :verbose, type: :boolean, default: false, desc: 'Verbose option'
    def check(url)
      unless Chekr::Validator.valid_url?(url)
        puts Chekr::Logger.error_url(url)
        return
      end
      Chekr::Logger.info_url(url)
      data = Chekr::Requester.perform(url: url, period: options[:period], interval: options[:interval], verbose: options[:verbose])
      Chekr::Logger.present(data)
    end
  end
end
