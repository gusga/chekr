require 'spec_helper'
require 'chekr/requester'

RSpec.describe Chekr::Requester do

  let(:test_url) { 'https://about.gitlab.com' }
  let(:response_list) { 
    [{time: 1.23, status: '200'},
     {time: 2.034, status: '200'},
     {time: 4.378, status: '200'}]
   }
  let(:times_list) { [1.23, 2.034, 4.378] }
  let(:perform_result) {
    { average_time: 2.547,
      max_time: 4.378,
      min_time: 1.23,
      interval: 1,
      statuses: { '200' => 3},
      url: 'https://about.gitlab.com',
      responses: response_list
    }
  }

  it '#response_formated' do
    stub_request(:get,test_url)
    expect(Chekr::Requester.response_formated(test_url)).to include(:status => '200')
  end

  context 'times' do
    it '#average_time' do
      expect(Chekr::Requester.average_time(times_list, times_list.size)).to eq(2.547)
    end
    it '#max_time' do
      expect(Chekr::Requester.max_time(times_list)).to eq(4.378)
    end
    it '#min_time' do
      expect(Chekr::Requester.min_time(times_list)).to eq(1.23)
    end
  end

  it '#status_code_list' do
    expect(Chekr::Requester.status_code_list(response_list)).to eq({'200' => 3})
  end

  it '#response_list' do
    stub_request(:get, test_url)
    list = Chekr::Requester.response_list(test_url,3,1,false)
    expect(list).not_to be_empty
    expect(list.size).to eq(3)
  end

  it '#perform' do
    stub_request(:get, test_url)
    allow(Chekr::Requester).to receive(:response_list) { response_list }
    expect(Chekr::Requester.perform(url:test_url, period:20, interval:1, verbose:false)).to eq(perform_result)
  end
end
