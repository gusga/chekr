require 'spec_helper'
require 'chekr/logger'

RSpec.describe Chekr::Logger do

  let(:test_url) { 'https://about.gitlab.com' }
  let(:response_list) {
    [{time: 1.23, status: '200'},
     {time: 2.034, status: '200'},
     {time: 4.378, status: '200'}]
   }

  let(:success_response) { { time: 1.23, status: '200' } }
  let(:error_response) { { time: 1.23, status: '400' } }
  let(:moved_response) { { time: 1.23, status: '301' } }

  let(:success_response_output_message){ "Response time 1.23s with status \e[32m200\e[0m\n" }
  let(:moved_response_output_message){ "Response time 1.23s with status \e[0m301\e[0m\n" }
  let(:error_response_output_message){ "Response time 1.23s with status \e[31m400\e[0m\n" }

  let(:info_url_output_message) { "Checking status of \e[32m#{test_url}\e[0m\n" }

  let(:status_codes_report) { "200: 3 Times\n400: 1 Times" }

  context '#info_request' do;
    context 'with status code' do
      it '2xx' do
        expect{ Chekr::Logger.info_request(success_response) }.to output(success_response_output_message).to_stdout
      end

      it '3xx' do
        expect{ Chekr::Logger.info_request(moved_response) }.to output(moved_response_output_message).to_stdout
      end

      it '4xx' do
        expect{ Chekr::Logger.info_request(error_response) }.to output(error_response_output_message).to_stdout
      end

    end
  end

  it '#info_url' do;
    expect{ Chekr::Logger.info_url(test_url) }.to output(info_url_output_message).to_stdout
  end

  it '#show_status_report' do
    expect(Chekr::Logger.show_status_report({'200' => 3, '400' => 1})).to eq(status_codes_report)
  end

end

