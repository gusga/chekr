require 'benchmark'
require 'httparty'

module Chekr #:nodoc:
  # Fetch info class
  module Requester
    extend self

    def response_formated(url)
      result = nil
      { time: Benchmark.realtime { result = ::HTTParty.get(url) }.round(3),
        status: "#{result.response.code}"
      }
    end

    def response_list(url, total_iteration, sleep_time, verbose=false)
      return if total_iteration < 1
      Array.new(total_iteration) do
        hsh = response_formated(url)
        Chekr::Logger.info_request(hsh) if verbose
        sleep(sleep_time)
        hsh
      end
    end

    def times_list(response_list)
      response_list.map{ |e| e[:time] }.compact
    end

    def average_time(list, total_iteration)
      (list.inject(:+) / total_iteration).round(3)
    end

    def min_time(list)
      list.min
    end

    def max_time(list)
      list.max
    end

    def status_code_list(response_list)
      response_list.inject({}) do |hsh, inner_hsh|
        code = inner_hsh[:status]
        hsh[code] = hsh[code].nil? ? 1 : hsh[code] + 1
        hsh
      end
    end

    # Public API
    # @params
    # url<String>: Website url
    # period<Fixnum/Integer>: Number of seconds the URL will be requested per minute
    # interval<Fixnum/Integer>: Number of minutes for the all operation
    # verbose<Bool>: verbose flag
    def perform(url:, period:, interval:, verbose: false)
      return if period < 1
      mins_to_secs = interval * 60
      total_iterations = (mins_to_secs / period).ceil
      list = response_list(url, total_iterations, period, verbose)
      return unless list
      t_list = times_list(list)
      { average_time: average_time(t_list, total_iterations),
        max_time: max_time(t_list).round(3),
        min_time: min_time(t_list).round(3),
        interval: interval,
        statuses: status_code_list(list),
        url: url,
        responses: list
      }
    end

  end
end
